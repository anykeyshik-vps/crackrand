#
#  Copyright (c) 2022. AnyKeyShik Rarity
#
#  admin@anykeyshik.xyz
#
#  https://t.me/AnyKeyShik
#

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, FileField, TextAreaField, PasswordField, BooleanField


class FlagForm(FlaskForm):
    flag = StringField()
    submit = SubmitField('Check flag!')


class TaskForm(FlaskForm):
    task_name = StringField('Task name')
    task_cond = TextAreaField('Task condition')
    task_credo = StringField('Task credo')
    task_file = FileField()
    task_flag = StringField('Task flag')
    task_enable = BooleanField("Enable")
    submit = SubmitField("Save task")


class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    remember_me = BooleanField("Remember me", default=True)
    submit = SubmitField('Login')
