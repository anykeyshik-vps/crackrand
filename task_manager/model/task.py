#
#  Copyright (c) 2022. AnyKeyShik Rarity
#
#  admin@anykeyshik.xyz
#
#  https://t.me/AnyKeyShik
#

from .. import db


class Task(db.Model):
    __tablename__ = 'tasks'

    id = db.Column(db.Integer, primary_key=True)  # primary keys are required by SQLAlchemy
    name = db.Column(db.String(1000))
    condition = db.Column(db.String(1000))
    credo = db.Column(db.String(1000))
    files = db.Column(db.String(5000))
    flag = db.Column(db.String(1000))
    enabled = db.Column(db.Boolean, unique=False, default=True)
