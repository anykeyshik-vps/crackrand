#
#  Copyright (c) 2022. AnyKeyShik Rarity
#
#  admin@anykeyshik.xyz
#
#  https://t.me/AnyKeyShik
#

UPLOAD_FOLDER = 'uploads'
DB_PATH = 'sqlite:///db.sql'
