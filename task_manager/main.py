#
#  Copyright (c) 2022. AnyKeyShik Rarity
#
#  admin@anykeyshik.xyz
#
#  https://t.me/AnyKeyShik
#


import os

from flask import Blueprint, render_template, flash, request, make_response, redirect, url_for, \
    current_app, send_from_directory
from flask_login import login_required

from . import db
from .model import FlagForm, TaskForm
from .model import Task

main = Blueprint('main', __name__)


@main.route('/', methods=['GET'])
def index():
    tasks = Task.query.filter_by(enabled=True)

    return render_template('index.html', tasks=tasks)


@main.route('/task', methods=['GET', 'POST'])
def task():
    id = request.args.get('id')

    task = Task.query.filter_by(id=id).first()
    if not task or task.enabled == False:
        return redirect(url_for('main.index'))

    form = FlagForm()

    if request.method == 'POST' and form.validate():
        user_flag = form.flag.data
        task_flag = task.flag

        check = request.cookies.get('task')
        if check == task_flag:
            flash('You\'re already solve this task!', 'success')
            return redirect(url_for('main.task', id=id))

        else:
            if user_flag == task_flag:
                flash('Great!', 'success')

                resp = make_response(redirect(url_for('main.task', id=id)))
                resp.set_cookie('task', task_flag)

                return resp
            else:
                flash('Not valid :c', 'danger')
                return redirect(url_for('main.task', id=id))

    return render_template('task_page.html', form=form, task=task)


@main.route('/admin', methods=['GET'])
@login_required
def admin():
    tasks = Task.query

    return render_template('admin.html', tasks=tasks)


@main.route('/edit', methods=['GET', 'POST'])
@login_required
def edit():
    id = request.args.get('id')
    form = TaskForm()
    exists = False

    task = Task.query.filter_by(id=id).first()
    if task:
        form.task_name.data = task.name
        form.task_cond.data = task.condition
        form.task_credo.data = task.credo
        form.task_flag.data = task.flag
        form.task_file.data = task.files
        form.task_enable.data = task.enabled
        exists = True

    if request.method == 'POST' and form.validate():
        form = TaskForm()

        filename = ''
        if form.task_file.data:
            filename = os.path.join(current_app.config['UPLOAD_FOLDER'], form.task_file.data.filename)
            form.task_file.data.save(os.path.join(current_app.root_path, filename))

        if not exists:
            task = Task(name=form.task_name.data, condition=form.task_cond.data, credo=form.task_credo.data,
                        files=filename, flag=form.task_flag.data, enabled=form.task_enable.data)
            db.session.add(task)
        else:
            Task.query.filter_by(id=id).update({'name': form.task_name.data, 'condition': form.task_cond.data,
                                                'credo': form.task_credo.data, 'files': filename,
                                                'flag': form.task_flag.data, 'enabled': form.task_enable.data})
        db.session.commit()

        flash("Changes were successfully saved", 'success')
        return redirect(url_for('main.admin'))

    return render_template('edit_page.html', form=form, task=task)


@main.route('/delete', methods=['GET'])
@login_required
def delete():
    id = request.args.get('id')
    task = Task.query.filter_by(id=id).first()

    db.session.delete(task)
    db.session.commit()

    flash("Task was successfully delete", 'success')
    return redirect(url_for('main.admin'))


@main.route('/uploads/<name>', methods=['GET'])
def download_file(name):
    return send_from_directory(current_app.config["UPLOAD_FOLDER"], name)
